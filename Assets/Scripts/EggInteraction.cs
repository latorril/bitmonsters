﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TouchScript.Gestures;
using TouchScript.Gestures.Simple;
using TouchScript.Hit;

public class EggInteraction : MonoBehaviour {
	
	public GameObject egg;
	public GameObject message;
	
	// Use this for initialization
	void Start () {
		this.GetComponent<PressGesture>().Pressed += pressedEvent;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void pressedEvent (object sender, EventArgs e) {
		egg.GetComponent<Animator>().Play("rock");
		message.GetComponent<Animator>().SetTrigger("show");
		}
}