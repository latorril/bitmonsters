﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TouchScript.Gestures;
using TouchScript.Gestures.Simple;
using TouchScript.Hit;

public class MonsterInteraction : MonoBehaviour {
	
	public GameObject monster;
	public GameObject message;

	// Use this for initialization
	void Start () {
		this.GetComponent<PressGesture>().Pressed += pressedEvent;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void pressedEvent (object sender, EventArgs e) {
		monster.GetComponent<Animator>().SetTrigger("foo");
		message.GetComponent<Animator>().SetTrigger("show");
	}
}