﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using TouchScript;
using TouchScript.Hit;
using TouchScript.Gestures;
using System;

public class Interaction : MonoBehaviour {
	
	//public GameObject _tapButtonObject;
	//Button _tapButton;

	public GameObject _messageObject;
	Text _message;
	
	public InputField _userInput;

	public GameObject _stage;
	public GameObject _egg;
	public GameObject _spy;
	public GameObject _cake;
	public GameObject _ball;
	public AudioSource _bicycleSong;
	bool _bicycle;
	bool _biketrigger;

	int _hatchTime;

	DateTime _now;
	public TimeSpan _rawAge;
	public MonsterData monsterData = new MonsterData();

	bool _running;
	Vector3 _startPosition;
	Vector3 _endPosition;
	
/*
	private void tappedHandler(object sender, EventArgs e)
	{
		var gesture = sender as TapGesture;
		ITouchHit hit;
		gesture.GetTargetHitResult(out hit);
		var hit3d = hit as ITouchHit3D;
		if (hit3d == null) return;
		
		Color color = new Color(Random.value, Random.value, Random.value);
		var cube = Instantiate(CubePrefab) as Transform;
		cube.parent = Container;
		cube.name = "Cube";
		cube.localScale = Vector3.one*Scale*cube.localScale.x;
		cube.position = hit3d.Point + hit3d.Normal*2;
		cube.renderer.material.color = color;
	}
*/

//int _day = System.DateTime.Now.DayOfYear;
	//int _hour = System.DateTime.Now.Hour;
	
	void Awake () {
		load();
		save();
	}

	void OnApplicationQuit() {
	}

	void Start () {
		
		_userInput.onEndEdit.AddListener(delegate {
			submitInput(_userInput.text);
		});
		//onClick.AddListener( xButtonPressed );

		_message = _messageObject.GetComponent<Text>();

/*
		_bicycle = false;
		_biketrigger = false;
*/

		_message.text = "";
		_hatchTime = 163400;
		
		_now = DateTime.Now;
		_rawAge = _now - Convert.ToDateTime(monsterData.birthday);
		hatchCheck();
		_stage.GetComponent<TapGesture>().Tapped += stageTapHandler;
		_running = false;
		//TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, false, false, true);
		TouchScreenKeyboard.hideInput = true;
	}

	public void submitInput(string userText){
		userText = userText.ToLower();

		if ( userText.IndexOf("eat") >= 0 || userText.IndexOf("cake") >= 0 ){
			_endPosition = new Vector3(_cake.transform.position.x, _startPosition.y, _cake.transform.position.z);
		}
		else if ( userText.IndexOf("ball") >= 0 || userText.IndexOf("play") >= 0 ){
			_endPosition = new Vector3(_ball.transform.position.x, _startPosition.y, _ball.transform.position.z);
		}
		
		_spy.GetComponent<Animator>().SetBool("running", true);
		_running = true;
		_userInput.text = "";
	}

	public void moveMonster( Vector3 start, Vector3 end){

		//CLEAN THIS UP!!!
		if (start != end && _running == true ){

			//move towards
			float speed = .75f;
			float turnSpeed = 5f;
			float delta = Time.deltaTime * speed;
			_spy.transform.position = Vector3.MoveTowards(start, end, delta);

			//rotate towards
			_spy.transform.rotation = Quaternion.Lerp (_spy.transform.rotation,  Quaternion.LookRotation(_spy.transform.position - _startPosition), delta * turnSpeed);

			_startPosition = _spy.transform.position;

		}

		else{
			_running = false;
			_spy.GetComponent<Animator>().SetBool("running", false);
		}
	}

	private void stageTapHandler(object sender, EventArgs e){
		var gesture = sender as TapGesture;
		ITouchHit hit;
		gesture.GetTargetHitResult(out hit);
		var hit3d = hit as ITouchHit3D;
		if (hit3d == null) return;
		_startPosition = _spy.transform.position;
		_endPosition = hit3d.Point;
		_running = true;
		_spy.GetComponent<Animator>().SetBool("running", true);

		//_spy.transform.position = hit3d.Point;
	}
	
	void Update () {
		hatchCheck();
		_now = DateTime.Now;
		_rawAge = _now - Convert.ToDateTime(monsterData.birthday);
		//Debug.Log( (int)_rawAge.TotalSeconds );
		moveMonster(_startPosition, _endPosition);
	}

/*
	public void playSong(){
		if ( _bicycle == true && _bicycleSong.isPlaying == true ) {
			_bicycleSong.Pause();
			_bicycle = false;
		}
		else if ( _bicycle == false && _bicycleSong.isPlaying == false ) {
			_bicycleSong.UnPause();
			_bicycle = true;
		}
	}
*/
	//put this in the monster data class ?
	public void hatchCheck(){

		if( (int)_rawAge.TotalSeconds >= _hatchTime ){
			
			_egg.SetActive(false);
			_spy.SetActive(true);
		}
		else{ 
			_egg.SetActive(true);
			_spy.SetActive(false);
		}
	}

	public void showMessage(){

/* Bicycle song
		if( _biketrigger == false){
			_biketrigger = true;
			_bicycle = true;
			_bicycleSong.Play();
		}
  */


		_message.text = "I am " + (int)_rawAge.TotalSeconds / 60 / 60 + " hours old... \nMy name is " + monsterData.name + ".";
		monsterData.taps ++;
		save();
	}

	public void save(){
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.OpenOrCreate);

		if( monsterData.birthday == null ){
			monsterData.birthday = DateTime.Now.ToString();
			Debug.Log("BIRTHDAY PARTY!");
		}

		bf.Serialize( file, monsterData);
		file.Close();

		Debug.Log("SAVED");
	}
	
	public void load(){
		if(File.Exists(Application.persistentDataPath + "/playerInfo.dat")){
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			MonsterData data = (MonsterData)bf.Deserialize(file);
			file.Close();
			
			monsterData.taps = data.taps;
			monsterData.birthday =  data.birthday;
			
			Debug.Log("LOADED");
			Debug.Log(monsterData.birthday);
		}
	}

	public void create(){
	}
}