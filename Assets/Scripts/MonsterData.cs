﻿using UnityEngine;
using System.Collections;

[System.Serializable] 
public class MonsterData {
	
	public int taps;
	public int age;
	public string birthday;
	public string name;
	
	public MonsterData () {
		this.name = "Azul";
	}
}
