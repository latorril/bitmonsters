﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraControl : MonoBehaviour {

	public GameObject target;
	public float yOffset;
	Vector3 aim;
	Vector3 offset;
	float magnitude;

	Vector3 startVal;

	
	float AccelerometerUpdateInterval;
	float LowPassKernelWidthInSeconds;

	private float LowPassFilterFactor;
	private float lowPassValue;


	// Use this for initialization
	void Start () {
		startVal = Input.acceleration;
		Input.gyro.enabled = true;
		magnitude = 10f;

		AccelerometerUpdateInterval = 1.0f / 60.0f;
		LowPassKernelWidthInSeconds = .5f;

		LowPassFilterFactor = AccelerometerUpdateInterval / LowPassKernelWidthInSeconds;

		lowPassValue = Input.acceleration.x;

	}

	void LateUpdate () {
		/*
		if( Input.acceleration.magnitude > 0 ){
			//use acellerometer to aim camera on device
			offset = new Vector3( -(LowPassFilterAccelerometer() - startVal.x) * magnitude, yOffset, 0);
		}
		else { 
			//if no acellerometer
			offset = new Vector3(0, yOffset, 0);
		}
		*/
		offset = new Vector3(0, yOffset, 0);
		aim = target.transform.position + offset;
		transform.LookAt( aim );
	}

	public float LowPassFilterAccelerometer(){
		lowPassValue = Mathf.Lerp(lowPassValue, Input.acceleration.x, LowPassFilterFactor);
		return lowPassValue;
	}
}
